import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputStringAnalyzor {
    String initial_input_Line;
     boolean correctionInput;
    static boolean checkSystemInfoDeltaI=false;
    static boolean checkSystemInfoDeltaV=false;
    static boolean checkSystemInfoDeltaT=false;
    static boolean checkSystemInfoMaxT=false;

    InputStringAnalyzor() {
    }
    static double unitsTransform(String initialNumberString){
        double outputNumber;
        String initialNumber = initialNumberString.replaceAll("s|A|V","");
        if (initialNumber.contains("G")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("G", ""));
            outputNumber *= Math.pow(10, 9);
            return outputNumber;
        } else if (initialNumber.contains("M")||initialNumber.contains("Meg")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("M|Meg", ""));
            outputNumber *= Math.pow(10, 6);
            return outputNumber;
        } else if (initialNumber.contains("K")||initialNumber.contains("k")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("K|k", ""));
            outputNumber *= Math.pow(10, 3);
            return outputNumber;
        } else if (initialNumber.contains("m")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("m", ""));
            outputNumber *= Math.pow(10, -3);
            return outputNumber;
        } else if (initialNumber.contains("u")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("u", ""));
            outputNumber *= Math.pow(10, -6);
            return outputNumber;
        } else if (initialNumber.contains("n")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("n", ""));
             outputNumber *= Math.pow(10, -9);
            return outputNumber;
        } else if (initialNumber.contains("p")) {
            outputNumber = Double.parseDouble(initialNumber.replaceAll("p", ""));
              outputNumber *= Math.pow(10, -12);
            return outputNumber;
        } else {
             outputNumber = Double.parseDouble(initialNumber);
            return outputNumber;
        }

    }
    void Analyze() throws Exception {
        correctionInput = false;
        if (initial_input_Line.indexOf("*")==0||initial_input_Line.indexOf("$")==0) {
            correctionInput = true;
        } else {
            String initial_input_Line1=initial_input_Line.replaceAll("sin","");
            String initial_input_Line2=initial_input_Line1.replaceAll("Vin","V1");
            String input_Line = initial_input_Line2.replaceAll("[ ]+", " ");
            String[] input_Analyzor = input_Line.split(" ");
            if((input_Analyzor[0].contains("I") && input_Analyzor.length==4 && !input_Analyzor[0].contains("d"))||
                    (input_Analyzor[0].contains("V") && input_Analyzor.length==4 && !input_Analyzor[0].contains("d"))){
                input_Line+=" "+"0"+" "+"0"+" "+"0";
                 input_Analyzor = input_Line.split(" ");
            }
            Pattern number = Pattern.compile("[0-9]");
            for (String s : input_Analyzor) {
                String checker = s.replaceAll("[a-z,A-Z]", "");
                Matcher m = number.matcher(checker);
                if (!m.find()) {
                    correctionInput = false;
                } else if (Double.parseDouble(checker) < 0) {
                    correctionInput = false;
                }
            }

            double quantity = 0;
            if (!input_Analyzor[0].contains("F") && !input_Analyzor[0].contains("G")
                    && !input_Analyzor[0].contains("E") && !input_Analyzor[0].contains("H")&&
                    !input_Analyzor[0].contains("d")&& !input_Analyzor[0].contains(".")) {
                quantity=InputStringAnalyzor.unitsTransform(input_Analyzor[3]);

                if (quantity < 0) {
                    correctionInput = false;
                }
            }
            if (input_Analyzor[0].contains("R")) {
                double resistance;
                int resistor_Number;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                resistance = quantity;
                if (input_Analyzor[0].replaceAll("R", "").isEmpty())
                    resistor_Number=1;
                else
                    resistor_Number = Integer.parseInt(input_Analyzor[0].replaceAll("R", ""));
                Resistor NewResistor = new Resistor(NewNode1, NewNode2, resistance, resistor_Number);
                NewResistor.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, NewResistor);
                NewNode2.addAdjacent(NewNode1, NewResistor);
                Branch.wholeBranchs.add(NewResistor);
                correctionInput = true;
            }
            else if (input_Analyzor[0].contains("C")) {
                double capacitance;
                int capacitor_Number;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                capacitance = quantity;
                if(input_Analyzor[0].replaceAll("C", "").isEmpty())
                    capacitor_Number=1;
                else
                    capacitor_Number = Integer.parseInt(input_Analyzor[0].replaceAll("C", ""));
                Capacitor Newcapacitor = new Capacitor(NewNode1, NewNode2, capacitance, capacitor_Number);
                Newcapacitor.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, Newcapacitor);
                NewNode2.addAdjacent(NewNode1, Newcapacitor);
                Branch.wholeBranchs.add(Newcapacitor);
                correctionInput = true;

            }
            else if (input_Analyzor[0].contains("L")) {
                double inductance;
                int inductor_Number;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                inductance = quantity;
                if(input_Analyzor[0].replaceAll("L", "").isEmpty())
                    inductor_Number=1;
                else
                    inductor_Number = Integer.parseInt(input_Analyzor[0].replaceAll("L", ""));;
                Inductor Newinductor = new Inductor(NewNode1, NewNode2, inductance, inductor_Number);
                Newinductor.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, Newinductor);
                NewNode2.addAdjacent(NewNode1, Newinductor);
                Branch.wholeBranchs.add(Newinductor);
                correctionInput = true;
            }
            ////////////////////for current source (should not be dI)
            else if (input_Analyzor[0].contains("I") && !input_Analyzor[0].contains("d")) {
                ////////////////////////////// I dc
                if (Double.parseDouble(input_Analyzor[4]) == 0) {
                    double idc;
                    int currentSource_Number;
                    Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                     Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                    idc = quantity;
                    currentSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("I", ""));
                    Idc Newidc = new Idc(NewNode1, NewNode2, idc, currentSource_Number);
                    Newidc.Branch_name = input_Analyzor[0];
                    NewNode1.addAdjacent(NewNode2, Newidc);
                    NewNode2.addAdjacent(NewNode1, Newidc);
                    Branch.wholeBranchs.add(Newidc);
                    correctionInput = true;
                }
                //////////////////////////////// I SIN
                else {
                    double idc;
                    double iamp = InputStringAnalyzor.unitsTransform(input_Analyzor[4]);
                    double iferq = InputStringAnalyzor.unitsTransform(input_Analyzor[5]);
                    double phase = InputStringAnalyzor.unitsTransform(input_Analyzor[6]);
                    int currentSource_Number;
                    Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                    idc = quantity;
                    currentSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("I", ""));
                    Isin Newisin = new Isin(NewNode1, NewNode2, idc, iamp, iferq, phase, currentSource_Number);
                    Newisin.Branch_name = input_Analyzor[0];
                    NewNode1.addAdjacent(NewNode2, Newisin);
                    NewNode2.addAdjacent(NewNode1, Newisin);
                    Branch.wholeBranchs.add(Newisin);
                    correctionInput = true;


                }

            }

            //////////////////// DEPENDENT CURRENT SOURCE (II)
            else if (input_Analyzor[0].contains("F")) {
                double gain;
                int currentSource_Number;
                Branch dependent = null;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                gain = Double.parseDouble(input_Analyzor[4]);
                currentSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("F", ""));
                for (Branch c : Branch.wholeBranchs) {
                    if (c.Branch_name.equals(input_Analyzor[3])) {
                        dependent = c;
                        break;
                    }
                }
                II NewII = new II(NewNode1, NewNode2, dependent, gain, currentSource_Number);
                NewII.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, NewII);
                NewNode2.addAdjacent(NewNode1, NewII);
                Branch.wholeBranchs.add(NewII);
                correctionInput = true;
            }
            //////////////////// DEPENDENT CURRENT SOURCE (IV)
            else if (input_Analyzor[0].contains("G")) {
                double gain;
                int currentSource_Number;
                Branch dependent = null;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                gain = Double.parseDouble(input_Analyzor[5]);
                currentSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("G", ""));
                for (Branch c : Branch.wholeBranchs) {
                    if (c.input.nodeNumber == Integer.parseInt(input_Analyzor[3]) &&
                            c.output.nodeNumber == Integer.parseInt(input_Analyzor[4])) {
                        dependent = c;
                        break;
                    }
                }
                IV NewIV = new IV(NewNode1, NewNode2, dependent, gain, currentSource_Number);
                NewIV.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, NewIV);
                NewNode2.addAdjacent(NewNode1, NewIV);
                Branch.wholeBranchs.add(NewIV);
                correctionInput = true;
            }
            //////////////////// DEPENDENT VOLTAGE SOURCE (VI)
            else if (input_Analyzor[0].contains("H")) {
                double gain;
                int voltageSource_Number;
                Branch dependent = null;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                gain = Double.parseDouble(input_Analyzor[4]);
                voltageSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("H", ""));
                for (Branch c : Branch.wholeBranchs) {
                    if (c.Branch_name.equals(input_Analyzor[3])) {
                        dependent = c;
                        break;
                    }
                }
                VI NewVI = new VI(NewNode1, NewNode2, dependent, gain, voltageSource_Number);
                voltageSourceError(NewVI);                                  /////EXCEPTION
                NewVI.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, NewVI);
                NewNode2.addAdjacent(NewNode1, NewVI);
                Branch.wholeBranchs.add(NewVI);
                correctionInput = true;
            }
            //////////////////// DEPENDENT VOLTAGE SOURCE (VV)
            else if (input_Analyzor[0].contains("E")) {
                double gain;
                int voltageSource_Number;
                Branch dependent = null;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                gain = Double.parseDouble(input_Analyzor[5]);
                voltageSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("E", ""));
                for (Branch c : Branch.wholeBranchs) {
                    if (c.input.nodeNumber == Integer.parseInt(input_Analyzor[3]) &&
                            c.output.nodeNumber == Integer.parseInt(input_Analyzor[4])) {
                        dependent = c;
                        break;
                    }
                }
                VV NewVV = new VV(NewNode1, NewNode2, dependent, gain, voltageSource_Number);
                voltageSourceError(NewVV);                                  /////EXCEPTION
                NewVV.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, NewVV);
                NewNode2.addAdjacent(NewNode1, NewVV);
                Branch.wholeBranchs.add(NewVV);
                correctionInput = true;
            }
            else if (input_Analyzor[0].contains("V") && !input_Analyzor[0].contains("d")) {
                ////////////////////////////// V dc
                if (Double.parseDouble(input_Analyzor[4]) == 0) {
                    double Vdc;
                    int voltageSource_Number;
                    Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                    Vdc = quantity;
                    voltageSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("V", ""));
                    Vdc Newvdc = new Vdc(NewNode1, NewNode2, Vdc, voltageSource_Number);
                    voltageSourceError(Newvdc);
                    NewNode1.addAdjacent(NewNode2, Newvdc);
                    NewNode2.addAdjacent(NewNode1, Newvdc);
                    Newvdc.Branch_name = input_Analyzor[0];
                    Branch.wholeBranchs.add(Newvdc);
                    correctionInput = true;

                }
                //////////////////////////////// V SIN
                else {
                    double vdc;
                    double vamp = InputStringAnalyzor.unitsTransform(input_Analyzor[4]);
                    double vferq = InputStringAnalyzor.unitsTransform(input_Analyzor[5]);
                    double phase = InputStringAnalyzor.unitsTransform(input_Analyzor[6]);
                    int voltageSource_Number;
                    Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                    Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                    vdc = quantity;
                    voltageSource_Number = Integer.parseInt(input_Analyzor[0].replaceAll("V", ""));
                    Vsin Newvsin = new Vsin(NewNode1, NewNode2, vdc, vamp, vferq, phase, voltageSource_Number);
                    voltageSourceError(Newvsin); ///////EXCEPTION
                    NewNode1.addAdjacent(NewNode2, Newvsin);
                    NewNode2.addAdjacent(NewNode1, Newvsin);
                    Newvsin.Branch_name = input_Analyzor[0];
                    Branch.wholeBranchs.add(Newvsin);
                    correctionInput = true;

                }
            }
            /////////////////// Diod
            else if (input_Analyzor[0].contains("D")) {
                int DiodeNumber;
                int DiodType;
                Node NewNode1 = Node.addNode(Integer.parseInt(input_Analyzor[1]),false);
                Node NewNode2 = Node.addNode(Integer.parseInt(input_Analyzor[2]),false);
                DiodType = (int) (quantity);
                DiodeNumber = Integer.parseInt(input_Analyzor[0].replaceAll("D", ""));
                Diode NewDiode = new Diode(NewNode1, NewNode2, DiodeNumber,DiodType);
                NewDiode.Branch_name = input_Analyzor[0];
                NewNode1.addAdjacent(NewNode2, NewDiode);
                NewNode2.addAdjacent(NewNode1, NewDiode);
                Branch.wholeBranchs.add(NewDiode);
                correctionInput = true;

            }
            ////////////////// systemeinfo
            if (input_Analyzor[0].equals("dV")||input_Analyzor[0].equals("dv")) {
                quantity=unitsTransform(input_Analyzor[1]);
                SystemInfo.deltaV = quantity;
                correctionInput = true;
                checkSystemInfoDeltaV = true;
            }
            if (input_Analyzor[0].equals("dT")||input_Analyzor[0].equals("dt")) {

                quantity=unitsTransform(input_Analyzor[1]);
                SystemInfo.deltaT = quantity;
                correctionInput = true;
                checkSystemInfoDeltaT = true;
            }
            if (input_Analyzor[0].equals("dI")||input_Analyzor[0].equals("di")) {

                quantity=unitsTransform(input_Analyzor[1]);
                SystemInfo.deltaI = quantity;
                correctionInput = true;
                checkSystemInfoDeltaI = true;
            }
            if (input_Analyzor[0].equals(".tran")) {

                quantity=unitsTransform(input_Analyzor[1]);
                SystemInfo.maxT = quantity;
                correctionInput = true;
                checkSystemInfoMaxT = true;
            }

        }
    }
        static String Hspice_Out (Branch element){
            String output;
            if (element instanceof Resistor) {
                output = "R" + ((Resistor) element).branchNumber + " " + ((Resistor) element).input.nodeNumber + " " +
                        ((Resistor) element).output.nodeNumber + " " + ((Resistor) element).resistance;
            } else if (element instanceof Inductor) {
                output = "L" + ((Inductor) element).branchNumber + " " + ((Inductor) element).input.nodeNumber + " " +
                        ((Inductor) element).output.nodeNumber + " " + ((Inductor) element).inductance;
            } else if (element instanceof Capacitor) {
                output = "C" + ((Capacitor) element).branchNumber + " " + ((Capacitor) element).input.nodeNumber + " " +
                        ((Capacitor) element).output.nodeNumber + " " + ((Capacitor) element).capacitance;
            } else if (element instanceof Diode) {
                output = "D" + ((Diode) element).DiodNumber + " " + ((Diode) element).input.nodeNumber + " " +
                        ((Diode) element).output.nodeNumber + " " + ((Diode) element).DiodType;
            } else if (element instanceof VI) {
                output = "H" + ((VI) element).branchNumber + " " + ((VI) element).input.nodeNumber + " " +
                        ((VI) element).output.nodeNumber + " " + ((VI) element).Dependent.Branch_name + " " +
                        ((VI) element).gain;
            } else if (element instanceof II) {
                output = "F" + ((II) element).branchNumber + " " + ((II) element).input.nodeNumber + " " +
                        ((II) element).output.nodeNumber + " " + ((II) element).Dependent.Branch_name + " " +
                        ((II) element).gain;
            } else if (element instanceof Isin) {
                output = "I" + ((Isin) element).branchNumber + " " + ((Isin) element).input.nodeNumber + " " +
                        ((Isin) element).output.nodeNumber + " " + ((Isin) element).idc + " "
                        + ((Isin) element).iamp + " " + ((Isin) element).freq + " " + ((Isin) element).phase;
            } else if (element instanceof Vsin) {
                output = "V" + ((Vsin) element).branchNumber + " " + ((Vsin) element).input.nodeNumber + " " +
                        ((Vsin) element).output.nodeNumber + " " + ((Vsin) element).vdc + " "
                        + ((Vsin) element).vamp + " " + ((Vsin) element).freq + " " + ((Vsin) element).phase;
            } else if (element instanceof Vdc) {
                output = "V" + ((Vdc) element).branchNumber + " " + ((Vdc) element).input.nodeNumber + " " +
                        ((Vdc) element).output.nodeNumber + " " + ((Vdc) element).vdc + " "
                        + "0" + " " + "0" + " " + "0";
            } else if (element instanceof Idc) {
                output = "I" + ((Idc) element).branchNumber + " " + ((Idc) element).input.nodeNumber + " " +
                        ((Idc) element).output.nodeNumber + " " + ((Idc) element).idc + " "
                        + "0" + " " + "0" + " " + "0";
            } else if (element instanceof VV) {
                output = "E" + ((VV) element).branchNumber + " " + ((VV) element).input.nodeNumber + " " +
                        ((VV) element).output.nodeNumber + " " + ((VV) element).Dependent.input.nodeNumber + " " +
                        ((VV) element).Dependent.output.nodeNumber + " " + ((VV) element).gain;

            } else if (element instanceof IV) {
                output = "G" + ((IV) element).branchNumber + " " + ((IV) element).input.nodeNumber + " " +
                        ((IV) element).output.nodeNumber + " " + ((IV) element).Dependent.input.nodeNumber + " " +
                        ((IV) element).Dependent.output.nodeNumber + " " + ((IV) element).gain;

            } else {
                return null;
            }///SHOULD PRINT ERROR
            return output;

        }
        void CurrentSourceError () throws Exception {
            for (Node J : Node.WholeNodes) {
                if (J.Adjacent.size() == 2) {
                    if (J.Adjacent.get(0).Branch instanceof Isource && J.Adjacent.get(1).Branch instanceof Isource) {
                        if (((Isource) J.Adjacent.get(0).Branch).getValue(SystemInfo.maxT) != ((Isource) J.Adjacent.get(1).Branch).getValue(SystemInfo.maxT)) {
                            throw new Exception("-2");
                        } else if (((Isource) J.Adjacent.get(0).Branch).getValue(SystemInfo.maxT / 2) != ((Isource) J.Adjacent.get(1).Branch).getValue(SystemInfo.maxT / 2)) {
                            throw new Exception("-2");
                        } else if (((Isource) J.Adjacent.get(0).Branch).getValue(SystemInfo.maxT / 4) != ((Isource) J.Adjacent.get(1).Branch).getValue(SystemInfo.maxT / 4)) {
                            throw new Exception("-2");
                        } else if (((Isource) J.Adjacent.get(0).Branch).getValue(SystemInfo.maxT / 8) != ((Isource) J.Adjacent.get(1).Branch).getValue(SystemInfo.maxT / 8)) {
                            throw new Exception("-2");
                        }
                    }
                }
            }
        }
        void voltageSourceError (Vsource x)throws Exception {
            for (Branch B : Branch.wholeBranchs) {
                if (B instanceof Vsource) {
                    if ((B.output.equals(x.input) && B.input.equals(x.output)) || (B.output.equals(x.output) && B.input.equals(x.input))) {
                        if (((Vsource) B).getValue(SystemInfo.maxT) != x.getValue(SystemInfo.maxT)) {
                            throw new Exception("-3");
                        } else if (((Vsource) B).getValue(SystemInfo.maxT / 2) != x.getValue(SystemInfo.maxT / 2)) {
                            throw new Exception("-3");
                        } else if (((Vsource) B).getValue(SystemInfo.maxT / 4) != x.getValue(SystemInfo.maxT / 4)) {
                            throw new Exception("-3");
                        } else if (((Vsource) B).getValue(SystemInfo.maxT / 8) != x.getValue(SystemInfo.maxT / 8)) {
                            throw new Exception("-3");
                        }
                    }
                }
            }
        }
        void GndError()throws Exception{
            try{
                Node Ground = Node.findFromeWholeNodes(0);
            }catch (Exception e){
                throw new Exception("-4");
            }

            for (Branch J :Branch.wholeBranchs){
                if (J instanceof Vsource){
                    if(Grand.WholeGrands.indexOf(J.input)!=-1 && Grand.WholeGrands.indexOf(J.output)!=-1){
                        throw  new Exception("-4");
                    }

                }
            }
        }
        void connectionGraphError () throws Exception {
            if (!Node.SingleNodes.isEmpty()) {
                throw new Exception("-5");
            }
        }


        void SystemeInfoError () throws Exception {
            if (!checkSystemInfoMaxT || !checkSystemInfoDeltaI ||
                    !checkSystemInfoDeltaT || !checkSystemInfoDeltaV) {
                if (!MainPanel.diChecker || !MainPanel.dtChecker || !MainPanel.dvChecker || !MainPanel.TimeChecker)
                     throw new Exception("-1");
            }
        }
        static void getInput (String InputString) throws Exception {
            String[] InputLines = InputString.split("\n");
            int lineNumber = 0;
            //should get initial string
            // while has nextline input
            InputStringAnalyzor inputStringAnalyzor = new InputStringAnalyzor();
            if(InputLines[InputLines.length-1].contains(".tran")){
                inputStringAnalyzor.initial_input_Line=InputLines[InputLines.length-1];
                try {
                    inputStringAnalyzor.Analyze();
                }catch (ArrayIndexOutOfBoundsException e){
                    throw new  Exception("wrong input ... Line :"+lineNumber);
                }

            }
            for (String inputLine : InputLines) {
                lineNumber++;
                inputStringAnalyzor.initial_input_Line = inputLine;
                try {
                    inputStringAnalyzor.Analyze();
                }catch (ArrayIndexOutOfBoundsException e){
                    throw new  Exception("wrong input ... Line :"+lineNumber);
                }
                inputStringAnalyzor.CurrentSourceError();
                if (!inputStringAnalyzor.correctionInput) {
                    System.out.println(lineNumber);
                    throw new  Exception("wrong input ... Line :"+lineNumber);
                }

            }
            inputStringAnalyzor.SystemeInfoError();
            inputStringAnalyzor.GndError();
            inputStringAnalyzor.connectionGraphError(); /////check nodes of circuit
        }

}