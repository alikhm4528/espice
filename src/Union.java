import java.util.ArrayList;
import java.util.HashSet;

public class Union {
    public static HashSet<Union> WholeUnions = new HashSet<>();
    Node Parent;
    ArrayList<Pack> Children;

    Union(Node Parent){
        Children = new ArrayList<>();
        this.Parent = Parent;
    }

    void merge(Union U, double voltage, double futureVoltage){
        for(Pack J : U.Children){
            Children.add(J);
        }
        Children.add(new Pack(U.Parent, voltage, futureVoltage));
        WholeUnions.remove(U);
    }

    Pack findChild(Node N) throws Exception{
        for(Pack J : Children){
            if(J.Node == N) return J;
        }
        throw new Exception("ERROR : Node not found");
    }

    static Union findFromWholeUnions(Node N) throws Exception{
        for(Union J : WholeUnions){
            if(J.Parent == N){
                return J;
            }
        }
        throw new Exception("ERROR : Union not found");
    }
}

class Pack {
    double voltage, futureVoltage;
    Node Node;

    Pack(Node Node,double voltage, double futureVoltage){
        this.Node = Node;
        this.voltage = voltage;
        this.futureVoltage = futureVoltage;
    }
}