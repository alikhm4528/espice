import java.util.ArrayList;
import java.util.HashSet;

public class Node extends SystemInfo{
    public static ArrayList<Node> WholeNodes = new ArrayList<>();
    // this is for input ERROR (number of single nodes)
    public static HashSet<Node> SingleNodes = new HashSet<>();
    // should handle it
    int nodeNumber;
    ArrayList<Pair> Adjacent;
    boolean visited = false;
    Node Parent = this;
    double[] Voltage;

    Node(int number) {
        Adjacent = new ArrayList<>();
        int len = giveLen();
        Voltage = new double[len + 1];
        nodeNumber = number;
    }

    public static Node findFromeWholeNodes(int number) throws Exception{
        for(Node J : WholeNodes){
            if(J.nodeNumber == number){
                return J;
            }
        }
        throw new Exception("ERROR : Node not found");
    }

    public static Node addNode(int number, boolean isGrand){
        try{
            Node FoundNode = findFromeWholeNodes(number);
            if(SingleNodes.contains(FoundNode)){
                SingleNodes.remove(FoundNode);
            }
            return FoundNode;
        }catch(Exception e){
            Node NewNode;
            if(isGrand == false){
                NewNode = new Node(number);
            }else{
                NewNode = new Grand(number);
                Grand.WholeGrands.add((Grand) NewNode);
            }
            WholeNodes.add(NewNode);
            SingleNodes.add(NewNode);
            return NewNode;
        }
    }

    void addAdjacent(Node newNode, Branch newBranch){
        Pair newPair = new Pair(newNode, newBranch);
        Adjacent.add(newPair);
    }

    void updateCurrent(int t){

        for(Pair J : Adjacent){
            Branch B = J.Branch;
            // if(B.visited == false){
                if(B instanceof Resistor){
                    Resistor R = (Resistor) B;
                    B.Current[t] = B.giveVoltage(t, this) / R.resistance;
                    // if(B.Branch_name.equals("R1"))
                        // System.out.println(B.input.Voltage[t-1] + " " + B.output.Voltage[t] + " " + B.output.nodeNumber + " " + t);
                }else if(B instanceof Capacitor){
                    Capacitor C = (Capacitor) B;
                    B.Current[t] = (B.giveVoltage(t, this) - B.giveVoltage(t-1, this)) * C.capacitance / deltaT;
                }else if(B instanceof Inductor){
                    Inductor L = (Inductor) B;
                    B.Current[t] = B.Current[t-1] + B.giveVoltage(t, this) * deltaT / L.inductance;
                }else if(B instanceof Isource){
                    Isource I = (Isource) B;
                    B.Current[t] = I.getValue(t * deltaT);
                }else{
                    // no need
                }
            // }
        }
    }

    // first of all start points should be generated
    public static void start(Node V){
        V.visited = true;
        for(Pair J : V.Adjacent){
            Branch B = J.Branch;
            Node U = J.Node;

            if(B instanceof Vsource && U.visited == false){
                Vsource Vs = (Vsource) B;
                U.Voltage[0] = V.Voltage[0] + Vs.getValue(0);
                U.Voltage[1] = V.Voltage[1] + Vs.getValue(deltaT);
                // System.out.println(U.Voltage[0] + " " + U.Voltage[1] + " " + B.giveVoltage(1, U) + " " + B.Branch_name + " " + U.nodeNumber);
            }else if(B instanceof Resistor){
                Resistor R = (Resistor) B;
                B.Current[0] = B.giveVoltage(1, V) / R.resistance;
                B.Current[1] = B.giveVoltage(1, V) / R.resistance;
            }else if(B instanceof Isource){
                Isource I = (Isource) B;
                B.Current[0] = I.getValue(0);
                B.Current[1] = I.getValue(deltaT);
            }

            if(U.visited == false){
                start(U);
            }
        }
    }

    double calculateCurrent(int t,double voltage){
        double current = 0;
        Voltage[t] = voltage;
        for(Pair J : Adjacent){
            Branch B = J.Branch;
            double value = 0;
            if(B instanceof Resistor){
                Resistor R = (Resistor) B;
                value = B.giveVoltage(t, this) / R.resistance;
                // System.out.println(value + "&&");
            }else if(B instanceof Capacitor){
                Capacitor C = (Capacitor) B;
                value = (B.giveVoltage(t, this) - B.giveVoltage(t-1, this)) * C.capacitance / deltaT;
                // System.out.println(value + "((");
            }else if(B instanceof Inductor){
                Inductor L = (Inductor) B;
                double tmp = (B.giveVoltage(t, this)) * deltaT / L.inductance;
                value = tmp + L.Current[t-1];
                // System.out.println(value + "%%");
            }else if(B instanceof Isource){
                Isource I = (Isource) B;
                value = I.Current[t-1];
                // System.out.println(value + "@@");
            }else if(B instanceof Vsource){
                // current wont add
            }else{
                // working ...
            }
            if(this == B.output){
                current -= value;
            }else{
                current += value;
            }
        }
        // System.out.println(current + "%%");
        return current;
    }

    // it is a function for check if we should add deltav or subtract
    Tuple getDeltavStatus(int t, double voltage1, double voltage2){
        double i1 = 0;
        Voltage[t+1] = (voltage1 + voltage2) / 2;
        for(Pair J : Adjacent){
            Branch B = J.Branch;
            double value = 0;
            if(B instanceof Inductor){
                Inductor L = (Inductor) B;
                value = (B.giveVoltage(t+1, this)) * deltaT / L.inductance;
            }
            if(this == B.output)
                i1 += B.Current[t] + value;
            else
                i1 -= B.Current[t] + value;
        }
        // System.out.println(voltage1 + " ************ " + voltage2);
        double i2 = calculateCurrent(t+1, voltage1);
        double i3 = calculateCurrent(t+1, voltage2);
        return new Tuple(i1, i2, i3);
    }

    // going forward one step
    static void iterate(int t, Union U){
        // should handle this Grand
        if(U.Parent.nodeNumber != 0){
            double before = U.Parent.Voltage[t];
            Tuple MainTuple = U.Parent.getDeltavStatus(t, before + deltaV, before - deltaV);
            for(Pack J : U.Children){
                Node ChildNode = J.Node;
                double vsourceValue = J.voltage + U.Parent.Voltage[t];
                MainTuple.add(ChildNode.getDeltavStatus(t,vsourceValue + deltaV, vsourceValue - deltaV));
            }
            double delta = (Math.abs(MainTuple.i1) - Math.abs(MainTuple.i2)) * deltaV / deltaI;
            System.out.println(U.Parent.nodeNumber + "   " + U.Parent.Voltage[t] + "   " + delta);
            MainTuple.print();

            // U.Parent.Voltage[t+1] = U.Parent.Voltage[t] + delta;
            // /*
            if(Math.abs(MainTuple.i1) > Math.abs(MainTuple.i2)){
                if(Math.abs(MainTuple.i2) > Math.abs(MainTuple.i3)){
                    U.Parent.Voltage[t+1] = U.Parent.Voltage[t] - deltaV;
                }else{
                    U.Parent.Voltage[t+1] = U.Parent.Voltage[t] + deltaV;
                }
            }else if(Math.abs(MainTuple.i1) > Math.abs(MainTuple.i3)){
                U.Parent.Voltage[t+1] = U.Parent.Voltage[t] - deltaV;
            }else{
                U.Parent.Voltage[t+1] = U.Parent.Voltage[t];
            }
            // */
            U.Parent.updateCurrent(t+1);
            for(Pack J : U.Children){
                J.Node.updateCurrent(t+1);
            }
        }

        U.Parent.updateCurrent(t+1);
        for(Pack J : U.Children){
            Node ChildNode = J.Node;
            ChildNode.Voltage[t+1] = U.Parent.Voltage[t+1] + J.futureVoltage;
            ChildNode.updateCurrent(t+1);
        }
        // should fix Currents on Branches and handle the whole process on iteration
    }

    Node getParent(){
        if(Parent == this)  return this;
        else    return Parent = Parent.getParent();
    }

    static void prepare(){
        Union.WholeUnions.clear();
        for(Node U : WholeNodes){
            U.visited = false;
            Union.WholeUnions.add(new Union(U));
            U.Parent = U;
        }
    }

    // this makes Unions
    void dfs(int t){
        visited = true;
        for(Pair J : Adjacent){
            Node U = J.Node;
            Branch B = J.Branch;
            if(B instanceof Vsource){
                Node GParent1 = getParent();
                Node GParent2 = U.getParent();
                if(GParent1 != GParent2){
                    if(GParent1.nodeNumber > GParent2.nodeNumber){
                        Node Tmp = GParent1;
                        GParent1 = GParent2;
                        GParent2 = Tmp;
                    }
                    try{
                        Union GU1 = Union.findFromWholeUnions(GParent1);
                        Union GU2 = Union.findFromWholeUnions(GParent2);

                        Vsource V = (Vsource) B;
                        double elementVoltage = V.getValue(t * deltaT);
                        double futureElementVoltage = V.getValue((t+1) * deltaT);
                        
                        if(this == V.input){
                            elementVoltage = -elementVoltage;
                            futureElementVoltage = -futureElementVoltage;
                        }

                        double voltage;
                        double futureVoltage;
                        try{
                            Pack FoundPack = GU1.findChild(this);
                            voltage = elementVoltage + FoundPack.voltage;
                            futureVoltage = futureElementVoltage + FoundPack.voltage;
                        }catch(Exception e1){
                            voltage = elementVoltage;
                            futureVoltage = futureElementVoltage;
                        }
                        GU1.merge(GU2, voltage, futureVoltage);
                    }catch(Exception e2){
                        System.out.println(e2);
                    }
                    GParent2.Parent = GParent1;
                }
            }
            if(U.visited == false){
                U.dfs(t);
            }
        }
    }

    static Branch findBranch(String Name) throws Exception{
        for(Node N : WholeNodes){
            for(Pair P : N.Adjacent){
                if(P.Branch.Branch_name.equals(Name)){
                    return P.Branch;
                }
            }
        }
        throw new Exception("ERROR : Branch not found");
    }
}

class Tuple {
    double i1 = 0,i2 = 0,i3 = 0;
    Tuple(double i1, double i2, double i3){
        this.i1 = i1;
        this.i2 = i2;
        this.i3 = i3;
    }

    void add(Tuple T){
        i1 += T.i1;
        i2 += T.i2;
        i3 += T.i3;
    }

    void print(){
        System.out.println(i1 + " * " + i2 + " * " + i3);
    }
}

// this is for storing adjacents in Node
class Pair{
    Branch Branch;
    Node Node;

    Pair(Node Node, Branch Branch){
        this.Node = Node;
        this.Branch = Branch;
    }
}