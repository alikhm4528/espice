abstract class Isource extends Branch{
    Isource(Node input,Node output){
        super(input,output);
    }

    abstract double getValue(double time);
}
class Isin extends Isource{
    double idc,iamp,freq,phase;
    Isin(Node input,Node output,double idc,double iamp,double freq,double phase,int branchNumber){
        super(input,output);
        this.idc = idc;
        this.iamp = iamp;
        this.freq = freq;
        this.phase=phase;
        this.branchNumber = branchNumber;
    }

    @Override
    public double getValue(double time) {
        return idc + iamp * Math.sin(2 * Math.PI * freq * time + phase);
    }
}
class Idc extends Isource{
    double idc;
    Idc(Node input,Node output,double idc,int branchNumber){
        super(input,output);
        this.idc = idc;
        this.branchNumber = branchNumber;
    }

    @Override
    public double getValue(double time) {
        return idc;
    }
}

class II extends Isource{
    double gain;
    Branch Dependent;
    II(Node input,Node output,Branch Dependent,double gain,int branchNumber){
        super(input,output);
        this.gain = gain;
        this.Dependent = Dependent;
        this.branchNumber = branchNumber;
    }

    @Override
    double getValue(double time) {
        return Dependent.Current[SystemInfo.giveIndex(time)-1] * gain;
    }
}
class IV extends Isource{
    double gain;
    Branch Dependent;
    IV(Node input,Node output,Branch Dependent,double gain,int branchNumber){
        super(input,output);
        this.gain = gain;
        this.Dependent= Dependent;
        this.branchNumber = branchNumber;
    }

    @Override
    double getValue(double time) {
        // check the index
        // needs more work ...
        int t = SystemInfo.giveIndex(time);
        return (Dependent.input.Voltage[t-1] - Dependent.output.Voltage[t-1]) * gain;
    }
}