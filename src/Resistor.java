public class Resistor extends Branch{
    double resistance;
    Resistor(Node input,Node output,double resistance,int branchNumber){
        super(input,output);
        this.resistance = resistance;
        this.branchNumber = branchNumber;

    }
}