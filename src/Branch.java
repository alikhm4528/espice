import java.util.ArrayList;

public class Branch {
    public static ArrayList<Branch> wholeBranchs = new ArrayList<>();
    boolean visited = false;
    String Branch_name;
    int branchNumber;
    Node input,output;
    double[] Current;
    Branch(Node input,Node output){
        this.input = input;
        this.output = output;
        int len = SystemInfo.giveLen();
        Current = new double[len + 1];
    }

    double giveVoltage(int t, Node ThisNode){
        if(input == ThisNode)
            return input.Voltage[t] - output.Voltage[t-1];
        else
            return input.Voltage[t-1] - output.Voltage[t];
    }

    double giveMax(int state){
        double ans = 0;
        for(int t = 1; t < SystemInfo.giveLen(); t++){
            double value;
            if(state == 0){
                value = giveVoltage(t, input);
            }else if(state == 1){
                value = Current[t];
            }else{
                value = Current[t] * (giveVoltage(t, input));
            }
            ans = Math.max(Math.abs(value), ans);
        }
        return ans;
    }
}