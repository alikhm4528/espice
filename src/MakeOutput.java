import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MakeOutput {

    static void make(String Path){
        try{
            BufferedWriter Writer = new BufferedWriter(new FileWriter(Path));
            Writer.write("Nodes : " + Node.WholeNodes.size() + "\n");
            for(Node N : Node.WholeNodes){
                String Output = N.nodeNumber + " : ";
                for(int t = 1; t < SystemInfo.giveLen(); t++){
                    Output += N.Voltage[t] + " ";
                }
                Output += "\n";
                Writer.write(Output);
            }
            Writer.write("\n\nBranches : " + Branch.wholeBranchs.size() + "\n");
            for(Branch B : Branch.wholeBranchs){
                String Output = B.Branch_name + " :\n\t";
                for(int t = 1; t < SystemInfo.giveLen(); t++){
                    Output += (B.giveVoltage(t, B.input)) + " ";
                }
                Output += "\n\t";
                for(int t = 1; t < SystemInfo.giveLen(); t++){
                    Output += B.Current[t] + " ";
                }
                Output += "\n\t";
                for(int t = 1; t < SystemInfo.giveLen(); t++){
                    Output += ((B.giveVoltage(t, B.input)) * B.Current[t]) + " ";
                }
                Output += "\n";
                Writer.write(Output);
            }
            Writer.close();
        }catch(IOException e){
            System.out.println(e);
        }
    }

    static void saveFile(String Path, String Content){
        try{
            FileWriter Writer = new FileWriter(Path);
            Writer.write(Content);
            Writer.close();
        }catch(IOException e){
            System.out.println(e);
        }
    }
}