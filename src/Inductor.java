public class Inductor extends Branch{
    double inductance;
    Inductor(Node input, Node output, double inductance,int branchNumber){
        super(input,output);
        this.inductance = inductance;
        this.branchNumber = branchNumber;
    }
}