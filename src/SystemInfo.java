public class SystemInfo {
    public static double deltaT, maxT, deltaV, deltaI;

    public static int giveLen(){
        int len = ((int) (maxT / deltaT)) + 1;
        return len;
    }

    public static int giveIndex(double time){
        int index = Math.abs((int) ( time / deltaT));
        return index;
    }
    
    public static double giveTime(int t){
        return deltaT * (t+1);
    }

    public static void initialize(){
        Branch.wholeBranchs.clear();
        Grand.WholeGrands.clear();
        InputStringAnalyzor.checkSystemInfoDeltaI=false;
        InputStringAnalyzor.checkSystemInfoDeltaV=false;
        InputStringAnalyzor.checkSystemInfoDeltaT=false;
        InputStringAnalyzor.checkSystemInfoMaxT=false;
        MainPanel.diChecker = false;
        MainPanel.dtChecker = false;
        MainPanel.dvChecker = false;
        Node.WholeNodes.clear();
        Node.SingleNodes.clear();
        Solve.isSolved = false;
        Union.WholeUnions.clear();
    }
}