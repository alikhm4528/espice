import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OutputWindow extends JPanel {
    JFrame MainFrame;
    JComboBox<String> Type;
    int type = 0;
    Branch Found;
    Draw Chart;
    JLabel Name;

    OutputWindow() {
        MainFrame = new JFrame("Draw");
        MainFrame.setSize(900, 600);
        MainFrame.setLayout(null);
        MainFrame.setIconImage(new ImageIcon("../Images/baba.jpg").getImage());
        String[] TypeList = { "Voltage", "Current", "Power" };
        Type = new JComboBox<>(TypeList);
        Type.setBounds(10, 5, 100, 20);
        MainFrame.add(Type);
        Type.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                type = Type.getSelectedIndex();
                Chart.setVisible(false);
                Chart = new Draw(Found, type);
                Chart.setBounds(10,30,870,530);
                Chart.setBackground(Color.lightGray);
                MainFrame.add(Chart);
            }
        });

        String ElementName = JOptionPane.showInputDialog(MainFrame, "Enter element name");
        Name = new JLabel(ElementName);
        Name.setBounds(120,5,100,20);
        MainFrame.add(Name);

        try{
            Found = Node.findBranch(ElementName);
            Chart = new Draw(Found, type);
            Chart.setBounds(10,30,870,530);
            Chart.setBackground(Color.lightGray);
            MainFrame.add(Chart);
            MainFrame.setVisible(true);
        }catch(Exception e){
            e.printStackTrace();
            if(ElementName != null){
                JOptionPane.showMessageDialog(MainFrame, "Element not found", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
            MainFrame.setVisible(false);
        }
    }

    public void paint(Graphics g){
        g.drawLine(10,10,20,20);
    }
}

class Draw extends JPanel{
    Branch Branch;
    int type;
    int margin = 10, zero = 530 / 2, length = 870 - 2 * margin;
    double max;

    Draw(Branch Branch, int type){
        this.Branch = Branch;
        this.type = type;
        max = Branch.giveMax(type);
    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        // 870 530
        drawChart(g);
        drawPixels(g);
    }

    void drawChart(Graphics g){
        g.drawLine(margin, zero, 870-margin, zero);
        g.drawLine(margin, margin, margin, 530-margin);
        g.drawString(Double.toString(max), margin, margin);
        g.drawString(Double.toString(-max), margin, 530-margin);
        String MaxT = Double.toString(SystemInfo.maxT);
        g.drawString(MaxT, 870-margin-MaxT.length()*5, zero+margin*2);
    }

    void drawPixels(Graphics g){
        g.setColor(Color.RED);
        int oldX = margin, oldY = zero;
        if(type == 0){
            oldY -= (int) ((Branch.input.Voltage[0] - Branch.output.Voltage[0]) / max * zero);
        }else if(type == 1){
            oldY -= (int) (Branch.Current[0] / max * zero);
        }else{
            int value = (int) ((Branch.input.Voltage[0] - Branch.output.Voltage[0]) * Branch.Current[0]);
            oldY -= (int) (value / max * zero);
        }

        for(int t = 1; t < SystemInfo.giveLen(); t++){
            double value;
            if(type == 0){
                value = Branch.giveVoltage(t, Branch.input);
            }else if(type == 1){
                value = Branch.Current[t];
            }else{
                value = (Branch.giveVoltage(t, Branch.input)) * Branch.Current[t];
            }
            int x = margin + (int) ((t * SystemInfo.deltaT) / SystemInfo.maxT * length);
            int y = zero - (int) (value / max * (zero - margin));
            // System.out.println(oldX + " " + oldY + " " + x + " " + y + " " + value);
            if(t > 1)  g.drawLine(oldX, oldY, x, y);
            oldX = x;
            oldY = y;
        }
        // System.out.println("************************************************************");
    }
}