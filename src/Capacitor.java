public class Capacitor extends Branch{
    double capacitance;
    Capacitor(Node input,Node output,double capacitance,int branchNumber){
        super(input,output);
        this.branchNumber = branchNumber;
        this.capacitance = capacitance;
    }
}