public class Diode extends Branch{
    int DiodNumber;
    int DiodType;
    Diode(Node input,Node output,int DiodNumber,int DiodType){
        super(input,output);
        this.DiodNumber=DiodNumber;
        this.DiodType=DiodType;
    }
}