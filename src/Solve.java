public class Solve {
    static boolean isSolved = false;

    static void run() throws Exception{
        // Grand Error -> have to fix (just one )
        // should handle new files
        isSolved = true;
        Node Node0 = Node.findFromeWholeNodes(0);
        Node.start(Node0);
        for(int t = 1; t < SystemInfo.giveLen(); t++){
            Node.prepare();
            Node0.dfs(t);
            for(Union U : Union.WholeUnions){
                Node.iterate(t, U);
            }
        }
    }
}