import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class MainPanel {
    JFrame MainFrame;
    JPanel LtiElement, Source, CurrentDependent, VoltageDependent;
    JLabel Component, Input, Output, Frequency, Value, Offset, Amplitude, LtiNumber, SourceNumber, Phase,
    Dependent, Gain, DependentInput, DependentOutput, Deltat, Deltav, Deltai, MaxTime;
    JMenuBar MenuBar;
    JMenu FileMenu, EditMenu, HelpMenu;
    JMenuItem New, Open, Save;
    JTextPane Editor;
    JScrollPane ScrollPane;
    JComboBox<String> ComponentCombo,VorICombo;
    JTextField InputField, OutputField, FrequencyField, ValueField, OffsetField, AmplitudeField, PhaseField,
    LtiNumberField, SourceNumberField, DependentField, GainField, DepInputField, DepOutputField,
    DeltatField, DeltavField, DeltaiField, MaxTimeField;
    JButton Add, Run, Draw, Chart;
    int element = 0, dependent = 0;
    static boolean dtChecker = false, diChecker = false, dvChecker = false, TimeChecker = false;
    File WorkingDirectory = FileSystemView.getFileSystemView().getHomeDirectory();

    MainPanel() {
        MainFrame = new JFrame();
        MainFrame.setIconImage(new ImageIcon("../Images/baba.jpg").getImage());
        MainFrame.setTitle("Espice");
        MainFrame.setBackground(Color.GRAY);
        MainFrame.setSize(1000, 700);
        MainFrame.setLayout(null);

        MenuBar = new JMenuBar();
        FileMenu = new JMenu("File");
        EditMenu = new JMenu("Edit");
        HelpMenu = new JMenu("Help");
        New = new JMenuItem("New");
        Open = new JMenuItem("Open");
        Save = new JMenuItem("Save");
        FileMenu.add(New);
        FileMenu.add(Open);
        FileMenu.add(Save);
        MenuBar.add(FileMenu);
        MenuBar.add(EditMenu);
        MenuBar.add(HelpMenu);
        MainFrame.setJMenuBar(MenuBar);
        Open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SystemInfo.initialize();
                JFileChooser FileChooser = new JFileChooser(WorkingDirectory);
                FileNameExtensionFilter Filter = new FileNameExtensionFilter("Text", "txt");
                FileChooser.setFileFilter(Filter);
                int state = FileChooser.showOpenDialog(null);
                File InputFile = FileChooser.getSelectedFile();
                String Content = "", Tmp;
                if (state == JFileChooser.APPROVE_OPTION) {
                    WorkingDirectory = FileChooser.getCurrentDirectory();
                    try {
                        BufferedReader Reader = new BufferedReader(new FileReader(InputFile.getPath()));
                        while ((Tmp = Reader.readLine()) != null) {
                            Content += Tmp + "\n";
                        }
                        Editor.setText(Content);
                        Reader.close();
                    } catch (Exception e1) {
                        System.out.println(e1);
                    }
                }
            }
        });

        Save.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser FileChoozer = new JFileChooser(WorkingDirectory);
                int state = FileChoozer.showSaveDialog(null);
                if(state == JFileChooser.APPROVE_OPTION){
                    WorkingDirectory = FileChoozer.getCurrentDirectory();
                    String Path = FileChoozer.getSelectedFile().getAbsolutePath();
                    MakeOutput.saveFile(Path, getContent());
                }
            }
        });

        New.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                Editor.setText(null);
                SystemInfo.initialize();
            }
        });
        
        // Lti Elements add panel
        LtiElement = new JPanel();
        LtiElement.setBounds(10,50,720,60);
        LtiElement.setLayout(null);
        MainFrame.add(LtiElement);
        LtiElement.setBorder(new LineBorder(Color.BLUE));
        Value = new JLabel("Value");
        Value.setBounds(10,5,60,20);
        LtiElement.add(Value);
        ValueField = new JTextField();
        ValueField.setBounds(80,5,70,22);
        LtiElement.add(ValueField);
        LtiNumber = new JLabel("Number");
        LtiNumber.setBounds(10,30,60,20);
        LtiElement.add(LtiNumber);
        LtiNumberField = new JTextField();
        LtiNumberField.setBounds(80,30,70,22);
        LtiElement.add(LtiNumberField);
        
        // Sources add Panel
        Source = new JPanel();
        Source.setBounds(10,50,720,60);
        MainFrame.add(Source);
        Source.setBorder(new LineBorder(Color.RED));
        Source.setVisible(false);
        Source.setLayout(null);
        Offset = new JLabel("Offset");
        Offset.setBounds(10,5,80,20);
        Source.add(Offset);
        OffsetField = new JTextField();
        OffsetField.setBounds(80,5,70,22);
        Source.add(OffsetField);
        Amplitude = new JLabel("Amplitude");
        Amplitude.setBounds(170,5,80,20);
        Source.add(Amplitude);
        AmplitudeField = new JTextField();
        AmplitudeField.setBounds(260,5,70,22);
        Source.add(AmplitudeField);
        Frequency = new JLabel("Frequency");
        Frequency.setBounds(350,5,80,20);
        Source.add(Frequency);
        FrequencyField = new JTextField();
        FrequencyField.setBounds(440,5,70,22);
        Source.add(FrequencyField);
        Phase = new JLabel("Phase");
        Phase.setBounds(10,30,60,20);
        Source.add(Phase);
        PhaseField = new JTextField();
        PhaseField.setBounds(80,30,70,22);
        Source.add(PhaseField);
        SourceNumber = new JLabel("Number");
        SourceNumber.setBounds(170,30,80,20);
        Source.add(SourceNumber);
        SourceNumberField = new JTextField();
        SourceNumberField.setBounds(260,30,70,22);
        Source.add(SourceNumberField);
        Gain = new JLabel("Gain");
        Gain.setBounds(350,30,80,20);
        Source.add(Gain);
        GainField = new JTextField();
        GainField.setBounds(440,30,70,22);
        Source.add(GainField);
        String[] VorI = {"Voltage", "Current"};
        VorICombo = new JComboBox<>(VorI);
        VorICombo.setBounds(600,20,80,30);
        Source.add(VorICombo);
        VorICombo.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                dependent = VorICombo.getSelectedIndex();
                if(dependent == 0){
                    // Voltage
                    VoltageDependent.setVisible(true);
                    CurrentDependent.setVisible(false);
                }else{
                    // Current
                    VoltageDependent.setVisible(false);
                    CurrentDependent.setVisible(true);
                }
            } 
        });

        CurrentDependent = new JPanel();
        CurrentDependent.setBounds(740,50,240,60);
        CurrentDependent.setLayout(null);
        CurrentDependent.setBorder(new LineBorder(Color.GREEN));
        MainFrame.add(CurrentDependent);
        Dependent = new JLabel("Dependent");
        Dependent.setBounds(10,5,80,20);
        CurrentDependent.add(Dependent);
        DependentField = new JTextField();
        DependentField.setBounds(120,5,70,22);
        CurrentDependent.add(DependentField);
        CurrentDependent.setVisible(false);

        VoltageDependent = new JPanel();
        VoltageDependent.setBounds(740,50,240,60);
        VoltageDependent.setLayout(null);
        VoltageDependent.setBorder(new LineBorder(Color.ORANGE));
        MainFrame.add(VoltageDependent);
        DependentInput = new JLabel("Input");
        DependentInput.setBounds(10,5,80,20);
        VoltageDependent.add(DependentInput);
        DepInputField = new JTextField();
        DepInputField.setBounds(120,5,70,22);
        VoltageDependent.add(DepInputField);
        DependentOutput = new JLabel("Output");
        DependentOutput.setBounds(10,30,80,20);
        VoltageDependent.add(DependentOutput);
        DepOutputField = new JTextField();
        DepOutputField.setBounds(120,30,70,22);
        VoltageDependent.add(DepOutputField);
        VoltageDependent.setVisible(false);
        
        Component = new JLabel("Component");
        Component.setBounds(10, 10, 90, 20);
        MainFrame.add(Component);
        String[] ComponentList = { "Resistor", "Inductor", "Capacitor", "Isource", "Vsource" };

        Deltat = new JLabel("Delta T");
        Deltat.setBounds(750,120,80,20);
        MainFrame.add(Deltat);
        DeltatField = new JTextField();
        DeltatField.setBounds(860,120,70,22);
        MainFrame.add(DeltatField);

        Deltav = new JLabel("Delta V");
        Deltav.setBounds(750,145,80,20);
        MainFrame.add(Deltav);
        DeltavField = new JTextField();
        DeltavField.setBounds(860,145,70,22);
        MainFrame.add(DeltavField);
        
        Deltai = new JLabel("Delta I");
        Deltai.setBounds(750,170,80,20);
        MainFrame.add(Deltai);
        DeltaiField = new JTextField();
        DeltaiField.setBounds(860,170,70,22);
        MainFrame.add(DeltaiField);

        MaxTime = new JLabel("Time");
        MaxTime.setBounds(750,195,80,20);
        MainFrame.add(MaxTime);
        MaxTimeField = new JTextField();
        MaxTimeField.setBounds(860,195,70,22);
        MainFrame.add(MaxTimeField);
        
        Input = new JLabel("Input");
        Input.setBounds(230, 10, 50, 20);
        MainFrame.add(Input);
        InputField = new JTextField();
        InputField.setBounds(280, 10, 60, 22);
        MainFrame.add(InputField);
        
        Output = new JLabel("Output");
        Output.setBounds(350, 10, 50, 20);
        MainFrame.add(Output);
        OutputField = new JTextField();
        OutputField.setBounds(410, 10, 60, 22);
        MainFrame.add(OutputField);
        
        ComponentCombo = new JComboBox<>(ComponentList);
        ComponentCombo.setBounds(105, 10, 110, 30);
        MainFrame.add(ComponentCombo);
        ComponentCombo.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                element = ComponentCombo.getSelectedIndex();
                if(element == 0){
                    LtiElement.setVisible(true);
                    Source.setVisible(false);
                    VoltageDependent.setVisible(false);
                    CurrentDependent.setVisible(false);
                }else if(element == 1){
                    LtiElement.setVisible(true);
                    Source.setVisible(false);
                    VoltageDependent.setVisible(false);
                    CurrentDependent.setVisible(false);
                }else if(element == 2){
                    LtiElement.setVisible(true);
                    Source.setVisible(false);
                    VoltageDependent.setVisible(false);
                    CurrentDependent.setVisible(false);
                }else if(element == 3){
                    LtiElement.setVisible(false);
                    Source.setVisible(true);
                    if(dependent == 0)
                        VoltageDependent.setVisible(true);
                    else
                        CurrentDependent.setVisible(true);
                }else if(element == 4){
                    LtiElement.setVisible(false);
                    Source.setVisible(true);
                    if(dependent == 0)
                        VoltageDependent.setVisible(true);
                    else
                        CurrentDependent.setVisible(true);
                }
            }
            
        });

        Add = new JButton("Add");
        Add.setBounds(490,10,70,30);
        Add.setBackground(Color.YELLOW);
        MainFrame.add(Add);
        Add.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String Line;
                Branch NewBranch;
                int pos = getLinePosition();
                try{
                    if(element < 3){
                        // Lti
                        double value = InputStringAnalyzor.unitsTransform(ValueField.getText());
                        int number = Integer.parseInt(LtiNumberField.getText());
                        int inputNumber = Integer.parseInt(InputField.getText());
                        int outputNumber = Integer.parseInt(OutputField.getText());
                        Node InputNode = new Node(inputNumber);
                        Node OutputNode = new Node(outputNumber);
                        if(element == 0){
                            // Resistor
                            NewBranch = new Resistor(InputNode, OutputNode, value, number);
                        }else if(element == 1){
                            // Inductor
                            NewBranch = new Inductor(InputNode, OutputNode, value, number);
                        }else{
                            // Capacitor element = 2
                            NewBranch = new Capacitor(InputNode, OutputNode, value, number);
                        }
                        Line = InputStringAnalyzor.Hspice_Out(NewBranch);
                    }else{
                        // Source
                        double offset;
                        int inputNumber = Integer.parseInt(InputField.getText());
                        int outputNumber = Integer.parseInt(OutputField.getText());
                        int number = Integer.parseInt(SourceNumberField.getText());
                        Node InputNode = new Node(inputNumber);
                        Node OutputNode = new Node(outputNumber);
                        if(!AmplitudeField.getText().equals("") || !FrequencyField.getText().equals("") ||
                        !PhaseField.getText().equals("")){
                            offset = InputStringAnalyzor.unitsTransform(OffsetField.getText());
                            double amp = InputStringAnalyzor.unitsTransform(AmplitudeField.getText());
                            double freq = InputStringAnalyzor.unitsTransform(FrequencyField.getText());
                            double phase = InputStringAnalyzor.unitsTransform(PhaseField.getText());
                            if(element == 3){
                                // Isource
                                NewBranch = new Isin(InputNode, OutputNode, offset, amp, freq, phase, number);
                            }else{
                                // Vsource element = 4
                                NewBranch = new Vsin(InputNode, OutputNode, offset, amp, freq, phase, number);
                            }
                        }else if((!DependentField.getText().equals("") && dependent == 1) ||
                        !GainField.getText().equals("") ||
                        (!DepInputField.getText().equals("") && dependent == 0) ||
                        (!DepOutputField.getText().equals("") && dependent == 0)){
                            double gain = InputStringAnalyzor.unitsTransform(GainField.getText());
                            Branch Found;
                            if(dependent == 0){
                                int innum = Integer.parseInt(DepInputField.getText());
                                int outnum = Integer.parseInt(DepOutputField.getText());
                                Node DepInput = new Node(innum);
                                Node DepOutput = new Node(outnum);
                                Found = new Branch(DepInput, DepOutput);
                            }else{
                                String DependentName = DependentField.getText();
                                Found = new Branch(InputNode, OutputNode);
                                Found.Branch_name = new String(DependentName);
                            }
                            if(element == 3){
                                // Isource
                                if(VorICombo.getSelectedIndex() == 0){
                                    // Voltage
                                    NewBranch = new IV(InputNode, OutputNode, Found, gain, number);
                                }else{
                                    // Current
                                    NewBranch = new II(InputNode, OutputNode, Found, gain, number);
                                }
                            }else{
                                // Vsource element = 4
                                if(VorICombo.getSelectedIndex() == 0){
                                    // Voltage
                                    NewBranch = new VV(InputNode, OutputNode, Found, gain, number);
                                }else{
                                    // Current
                                    NewBranch = new VI(InputNode, OutputNode, Found, gain, number);
                                }
                            }
                        }else{
                            offset = InputStringAnalyzor.unitsTransform(OffsetField.getText());
                            if(element == 3){
                                // Isource
                                NewBranch = new Idc(InputNode, OutputNode, offset, number);
                            }else{
                                // Vsource element = 4
                                NewBranch = new Vdc(InputNode, OutputNode, offset, number);
                            }
                        }
                        Line = InputStringAnalyzor.Hspice_Out(NewBranch);
                    }
                    Document doc = Editor.getDocument();
                    try{
                        if(Editor.getText().length() == pos && pos > 0){
                            doc.insertString(pos,"\n" + Line, null);
                        }else if(pos > 0){
                            doc.insertString(pos, Line + "\n", null);
                        }else{
                            doc.insertString(pos, Line, null);
                        }
                    }catch(BadLocationException e2){
                        JOptionPane.showMessageDialog(MainFrame, "Insert Error", "Error",
                        JOptionPane.ERROR_MESSAGE); 
                    }
                }catch(Exception e1){
                    JOptionPane.showMessageDialog(MainFrame, "Error occurred", "Error",
                    JOptionPane.ERROR_MESSAGE);
                }
            }            
        });

        Run = new JButton("Run");
        Run.setBounds(570,10,70,30);
        Run.setBackground(Color.GREEN);
        MainFrame.add(Run);
        Run.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                SystemInfo.initialize();
                try{
                    if (!DeltatField.getText().equals("")){
                        double deltaT = InputStringAnalyzor.unitsTransform(DeltatField.getText());
                        dtChecker=true;
                        SystemInfo.deltaT = deltaT;
                    }
                    if (!DeltavField.getText().equals("")){
                        double deltaV = InputStringAnalyzor.unitsTransform(DeltavField.getText());
                        dvChecker=true;
                        SystemInfo.deltaV = deltaV;
                    }
                    if (!DeltaiField.getText().equals("")){
                        double deltaI = InputStringAnalyzor.unitsTransform(DeltaiField.getText());
                        diChecker=true;
                        SystemInfo.deltaI = deltaI;
                    }
                    if (!MaxTimeField.getText().equals("")){
                        double maxTime = InputStringAnalyzor.unitsTransform(MaxTimeField.getText());
                        TimeChecker=true;
                        SystemInfo.maxT = maxTime;
                    }
                    InputStringAnalyzor.getInput(getContent());
                    int saveState =  JOptionPane.showConfirmDialog(MainFrame, "Do you want to save output file ?");
                    if(saveState == JOptionPane.YES_OPTION){
                        JFileChooser FileChooser = new JFileChooser(WorkingDirectory);
                        int state = FileChooser.showSaveDialog(null);
                        String Path;
                        if (state == JFileChooser.APPROVE_OPTION) {
                            WorkingDirectory = FileChooser.getCurrentDirectory();
                            Path = FileChooser.getSelectedFile().getAbsolutePath();
                            Solve.run();
                            MakeOutput.make(Path);
                        }
                    }else if(saveState == JOptionPane.NO_OPTION){
                        Solve.run();
                    }
                    // System.out.println(SystemInfo.deltaT + " " + SystemInfo.giveLen() + " " + SystemInfo.maxT + "\n**************************\n\n");
                }catch(Exception e1){
                    e1.printStackTrace();
                    String Message = e1.getMessage();
                    if(e1 instanceof NumberFormatException){
                        Message = "-1";
                    }
                    JOptionPane.showMessageDialog(MainFrame, Message, "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        Draw = new JButton("Draw");
        Draw.setBounds(650,10,80,30);
        Draw.setBackground(Color.magenta);
        MainFrame.add(Draw);
        Draw.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                Madar NewMadar = new Madar();
                NewMadar.draw(getContent());
            }
        });

        Chart = new JButton("Chart");
        Chart.setBounds(740,10,80,30);
        Chart.setBackground(Color.lightGray);
        MainFrame.add(Chart);
        Chart.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Solve.isSolved == true){
                    OutputWindow DrawWindow = new OutputWindow();
                }else{
                    JOptionPane.showMessageDialog(MainFrame, "You should run first", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } 
        });
        
        Editor = new JTextPane();
        Editor.setBorder(new LineBorder(Color.BLACK));
        ScrollPane = new JScrollPane(Editor);
        ScrollPane.setBounds(10,120,720,520);
        MainFrame.add(ScrollPane);

        MainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainFrame.setVisible(true);
    }

    String getContent(){
        return Editor.getText();
    }

    int getLinePosition(){
        int pos = Editor.getCaretPosition();
        while(pos != Editor.getText().length() && Editor.getText().charAt(pos) != '\n'){
            pos++;
        }
        if(pos != Editor.getText().length())    pos++;
        return pos;
    }
}