abstract class Vsource extends Branch{
    Vsource(Node input,Node output){
        // input is positive
        super(input,output);
    }

    abstract double getValue(double time);

}
class Vsin extends Vsource{
    double vdc,vamp,freq,phase;
    Vsin(Node input,Node output,double vdc,double vamp,double freq,double phase,int branchNumber){
        super(input,output);
        this.vdc = vdc;
        this.vamp = vamp;
        this.freq = freq;
        this.phase=phase;
        this.branchNumber = branchNumber;
    }

    @Override
    double getValue(double time){
        return vdc + vamp * Math.sin(2 * Math.PI * freq * time+phase);
    }
}
class Vdc extends Vsource{
    double vdc;
    Vdc(Node input,Node output,double vdc,int branchNumber){
        super(input,output);
        this.vdc = vdc;
        this.branchNumber = branchNumber;
    }

    @Override
    double getValue(double time){
        return vdc;
    }
}
class VI extends Vsource{
    double gain;
    Branch Dependent;
    VI(Node input,Node output,Branch Dependent,double gain,int branchNumber){
        super(input,output);
        this.gain = gain;
        this.Dependent = Dependent;
        this.branchNumber = branchNumber;
    }

    @Override
    double getValue(double time) {
        return Dependent.Current[SystemInfo.giveIndex(time)-1] * gain;
    }
}
class VV extends Vsource{
    double gain;
    Branch Dependent;
    VV(Node input,Node output,Branch Dependent,double gain,int branchNumber){
        super(input,output);
        this.gain = gain;
        this.Dependent = Dependent;
        this.branchNumber = branchNumber;
    }

    @Override
    double getValue(double time) {
        int t = SystemInfo.giveIndex(time);
        return (Dependent.input.Voltage[t] - Dependent.output.Voltage[t-1]) * gain;
    }
}